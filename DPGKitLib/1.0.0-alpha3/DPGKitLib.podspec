Pod::Spec.new do |s|
  s.name             = "DPGKitLib"
  s.version          = "1.0.0-alpha3"
  s.summary          = "DPG internal library"
  s.description      = <<-DESC
                       DPGKit library contains:
                       * Analytics (comScore + Stream Sense, Insight, SAC, Krux)
                       * DPG Account
                       DESC
  s.homepage         = "http://www.dpg.com"
  s.license          = 'Propietary'
  s.author           = { "Mate Csengeri" => "mate.csengeri@mondriaan.com" }
  s.source           = { :http => "https://pods.sanomamdc.com/DPGKitLib/DPGKitLib-1.0.0-alpha3.zip" }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.xcconfig = { 'HEADER_SEARCH_PATHS' => "${PODS_ROOT}/#{s.name}/DPGKitLib/**/*.h" }

  s.subspec 'Core' do |core|
    core.source_files = 'DPGKitLib/DPGKit_Core/*.h'
    core.vendored_libraries = 'DPGKitLib/DPGKit_Core/libDPGKit_Core.a'
    core.frameworks = 'Foundation', 'CoreLocation', 'AdSupport'
    core.dependency 'DPGKitLib/Helpers'
  end

  s.subspec 'Helpers' do |helpers|
    helpers.source_files = 'DPGKitLib/DPGKit_Helpers/*.h'
    helpers.vendored_libraries = 'DPGKitLib/DPGKit_Helpers/libDPGKit_Helpers.a'
  end

  s.subspec 'ComScore' do |comScore|
    comScore.source_files = 'DPGKitLib/DPGKit_ComScore/*.{h,m}'
    comScore.vendored_libraries = 'DPGKitLib/DPGKit_ComScore/libDPGKit_ComScore.a'
    comScore.dependency 'DPGKitLib/Core'
    comScore.dependency 'ComScore', '5.8.4'
  end

  s.subspec 'Krux' do |krux|
    krux.source_files = 'DPGKitLib/DPGKit_Krux/*.{h,m}'
    krux.vendored_libraries = 'DPGKitLib/DPGKit_Krux/libDPGKit_Krux.a'
    krux.vendored_frameworks = 'DPGKitLib/DPGKit_Krux/Krux/iOSKruxLibUniversal.framework'
    krux.dependency 'DPGKitLib/Core'
    krux.dependency 'DPGKitLib/Glimr'
    krux.frameworks = 'SystemConfiguration'
  end

  s.subspec 'SAC' do |sac|
    sac.source_files = 'DPGKitLib/DPGKit_SAC/*.{h,m}'
    sac.vendored_libraries = 'DPGKitLib/DPGKit_SAC/libDPGKit_SAC.a'
    sac.dependency 'DPGKitLib/Core'
  end

  s.subspec 'GoogleAnalytics' do |googleanalytics|
    googleanalytics.source_files = 'DPGKitLib/DPGKit_GoogleAnalytics/*.{h,m}'
    googleanalytics.vendored_libraries = 'DPGKitLib/DPGKit_GoogleAnalytics/libDPGKit_GoogleAnalytics.a'
    googleanalytics.dependency 'DPGKitLib/Core'
    googleanalytics.frameworks = 'AdSupport','CoreData','SystemConfiguration'
    googleanalytics.library = 'z','sqlite3'
  end

  s.subspec 'Chartbeat' do |chartbeat|
    chartbeat.source_files = 'DPGKitLib/DPGKit_Chartbeat/*.{h,m}'
    chartbeat.vendored_library = 'DPGKitLib/DPGKit_Chartbeat/libDPGKit_Chartbeat.a'
    chartbeat.dependency 'DPGKitLib/Core'
    chartbeat.dependency 'Chartbeat', '1.4.0'
  end

  s.subspec 'FirstUseExperience' do |firstuseexperience|
   firstuseexperience.source_files = 'DPGKitLib/DPGKit_FirstUseExperience/*.{h,m}'
   firstuseexperience.vendored_libraries = 'DPGKitLib/DPGKit_FirstUseExperience/libDPGKit_FirstUseExperience.a'
   firstuseexperience.ios.resource_bundle = {'DPGKitFUEResource' => 'DPGKitLib/DPGKit_FirstUseExperience/DPGKitFUEResource.bundle/*.nib'}
   firstuseexperience.frameworks = 'MediaPlayer'
   firstuseexperience.dependency 'DPGKitLib/Helpers'
  end

  s.subspec 'AppConfig' do |appconfig|
    appconfig.source_files = 'DPGKitLib/DPGKit_AppConfig/*.{h,m}'
    appconfig.vendored_libraries = 'DPGKitLib/DPGKit_AppConfig/libDPGKit_AppConfig.a'
  end

  s.subspec 'Update' do |update|
    update.source_files = 'DPGKitLib/DPGKit_Update/*.{h,m}'
    update.vendored_libraries = 'DPGKitLib/DPGKit_Update/libDPGKit_Update.a'
    update.dependency 'DPGKitLib/Core'
  end

  s.subspec 'DPGAccount' do |sanomaaccount|
    sanomaaccount.source_files = 'DPGKitLib/DPGKit_DPGAccount/*.{h,m}'
    sanomaaccount.vendored_libraries = 'DPGKitLib/DPGKit_DPGAccount/libDPGKit_DPGAccount.a'
    sanomaaccount.ios.resource_bundles = {'DPGKitAccountResource' => 'DPGKitLib/DPGKit_DPGAccount/DPGKitAccountResource.bundle/*.nib'}
    sanomaaccount.dependency 'DPGKitLib/Core'
    sanomaaccount.dependency 'DPGKitLib/Helpers'
    sanomaaccount.dependency 'DCKeyValueObjectMapping', '1.5'
    sanomaaccount.dependency 'AFNetworking/Reachability', '3.2.1'
    sanomaaccount.frameworks = 'AssetsLibrary','AddressBook','CoreLocation','CoreMotion','CoreGraphics','CoreText','MediaPlayer','Security','SystemConfiguration'
  end

  s.subspec 'AppReview' do |appreview|
    appreview.source_files = 'DPGKitLib/DPGKit_AppReview/*.{h,m}'
    appreview.vendored_libraries = 'DPGKitLib/DPGKit_AppReview/libDPGKit_AppReview.a'
    appreview.ios.resource_bundle = {'DPGKitAppReviewResource' => 'DPGKitLib/DPGKit_AppReview/DPGKitAppReviewResource.bundle/*.nib'}
    appreview.dependency 'DPGKitLib/Core'
  end

  s.subspec 'Privacy' do |privacy|
    privacy.source_files = 'DPGKitLib/DPGKit_Privacy/*.{h,m}'
    privacy.vendored_libraries = 'DPGKitLib/DPGKit_Privacy/libDPGKit_Privacy.a'
    privacy.ios.resource_bundle = {'DPGKitPrivacyResource' => 'DPGKitLib/DPGKit_Privacy/DPGKitPrivacyResource.bundle/*.{nib,png}'}
    privacy.dependency 'DPGKitLib/Core'
  end

  s.subspec 'Notifications' do |notifications|
    notifications.source_files = 'DPGKitLib/DPGKit_Notifications/*.{h,m}'
    notifications.vendored_library = 'DPGKitLib/DPGKit_Notifications/libDPGKit_Notifications.a'
    notifications.ios.resource_bundle = {'DPGKitNotificationsResource' => 'DPGKitLib/DPGKit_Notifications/DPGKitNotificationsResource.bundle/*.{nib,tiff}'}
    notifications.dependency 'DPGKitLib/Core'
  end

  s.subspec 'Glimr' do |glimr|
    glimr.source_files = 'DPGKitLib/DPGKit_Glimr/*.{h,m}'
    glimr.vendored_libraries = 'DPGKitLib/DPGKit_Glimr/libDPGKit_Glimr.a'
    glimr.dependency 'DPGKitLib/Core'
    glimr.dependency 'GLGeoRealtime', '3.0.6'
    glimr.frameworks = 'CoreTelephony', 'CoreBluetooth', 'SystemConfiguration', 'AdSupport', 'CoreLocation'
  end

  s.subspec 'AppNexus' do |appnexus|
    appnexus.source_files = 'DPGKitLib/DPGKit_AppNexus/*.{h,m}'
    appnexus.vendored_libraries = 'DPGKitLib/DPGKit_AppNexus/libDPGKit_AppNexus.a'
    appnexus.dependency 'DPGKitLib/Core'
    appnexus.dependency 'DPGKitLib/Glimr'
    appnexus.dependency 'AppNexusSDK', '7.8'
    appnexus.frameworks = 'AudioToolbox', 'AVFoundation', 'CFNetwork', 'CoreGraphics', 'CoreTelephony', 'EventKit', 'EventKitUI', 'Foundation', 'iAd', 'MediaPlayer', 'MessageUI', 'MobileCoreServices', 'PassKit', 'QuartzCore', 'Security', 'Social', 'StoreKit', 'SystemConfiguration', 'CoreMedia', 'AVKit'
    appnexus.library = 'z'
  end

  s.subspec 'FirstUseExperienceSE' do |firstuseexperiencese|
    firstuseexperiencese.source_files = 'DPGKitLib/DPGKit_FirstUseExperienceSE/*.{h,m}'
    firstuseexperiencese.vendored_libraries = 'DPGKitLib/DPGKit_FirstUseExperienceSE/libDPGKit_FirstUseExperienceSE.a'
    firstuseexperiencese.ios.resource_bundle = {'DPGKitFUESEResource' => 'DPGKitLib/DPGKit_FirstUseExperienceSE/DPGKitFUESEResource.bundle/*.nib'}
    firstuseexperiencese.dependency 'DPGKitLib/Helpers'
  end

end
